<?php if (isset($data['message'])): ?>
  <?php echo $data['message'] ?>
<?php endif ?>
<div>
<?php if (isset($data['config'])): ?>
  <div class="posts-most-viewed-box">
    <div class="postbox">
      <h2 class="hndle">Current Provider Credentials</h2>
      <div class="inside">
        <div class="form-wrap">
          <div class="form-field">
          <table border="0" cellpadding="5" cellspacing="0">
            <?php foreach ($data['config'] as $key => $value): ?>
              <tr>
                <td align="left" width="15%" valign="top">
                  <strong> <?php echo str_replace("_", ' ', $key)?>:</strong>
                </td width="85%">
                <td align="left">
                  <?php if (is_string($value)): ?>
                    <?php echo  $value ?>
                  <?php elseif(is_array($value)): ?>
                    <?php foreach ($value as $k => $v): ?>
                      <div><strong> <?php echo  $k; ?>:</strong> <?php echo $v; ?><br><br></div>
                    <?php endforeach ?>
                  <?php endif ?>
                <br></td>
              </tr>
            <?php endforeach ?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endif ?>
  <form action="" method="post">
    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">Check Provider Status</h2>
        <div class="inside">
          <div class="form-wrap">
            <div class="form-field">
              <?php if (isset($data['status'])): ?>
              <div  class="status_msg status-<?php  echo $data['status']['type']?>">
              <?php if ($data['status']['type'] == 'success'): ?>
                <div class="posts-most-viewed-info">There ins't any errors in your Provider Api</div>
                <div  class="status_more">
                  <a href="#" class="button button-primary js-posts-most-viewed-see-more">See more</a>
                  <div class="posts-most-viewed-see-more"><pre><?php print_r($data['status']['msg']) ?></pre></div>
                </div>
              <?php else: ?>
                <div class="posts-most-viewed-info">
                  <div class="posts-most-viewed-info-error">ERROR:</div>
                  <?php echo $data['status']['msg'] ?>
                </div>
              <?php endif; ?>
              </div>
              <?php else: ?>
              <input type="hidden" name="check_status_posts_most_viewed_action"  value="true"/>
              <input type="submit" value="Check" class="button button-primary" />
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

  <form action="" method="post">
    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">Delete Cache</h2>
          <div class="inside">
            <div class="form-wrap">
              <div class="form-field">
                <input type="hidden" name="delete_cache_posts_most_viewed_action" value="true"/>
                <input type="submit" value="Delete" class="button button-primary" />
              </div>
            </div>
          </div>
        </div>
      </div>
  </form>

  <form action="" method="post">
    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">Delete Provider Data</h2>
          <div class="inside">
            <div class="form-wrap">
              <div class="form-field">
                <input type="hidden" name="delete_posts_most_viewed_provider_data_action"  value="true"/>
                <input type="submit" value="Delete" class="button button-primary" />
              </div>
            </div>
          </div>
        </div>
      </div>
  </form>
</div>