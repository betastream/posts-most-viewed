<?php if (isset($data['message'])): ?>
  <?php echo $data['message'] ?>
<?php endif ?>

<form action="" method="post" enctype="multipart/form-data">
  <?php if (count($data['providers']) > 1): ?>
    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">Select the Provider</h2>
        <div class="inside">
          <div class="form-wrap">
            <div class="form-field">
              <select name="post_most_viewed_providers" class="js-posts-most-viewed-providers">
                <?php foreach ($data['providers'] as $key => $provider): ?>
                  <option value="<?php echo $key ?>" <?php (isset($_POST['post_most_viewed_providers'])) ? selected( $_POST['post_most_viewed_providers'], $key) : ''; ?>><?php echo $provider?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php else: reset($data['providers']); ?>
    <input type="hidden" value="<?php  echo key($data['providers']) ?>" name="post_most_viewed_providers">
  <?php endif ?>

  <!-- GOOGLE ANALYTICS -->
  <div data-posts-most-viewed-provider="google_provider" class="posts-most-viewed-provider <?php echo ($data['selected_provider'] == 'google_provider') ? 'selected' : '' ?>">
    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">How To Create a Google Analytics API Service</h2>
        <div class="inside">
          <div>
            <p>To create a Google Analytics API service and get your Google API credentials, follow the instructions on this  <a href="https://developers.google.com/analytics/devguides/reporting/core/v3/quickstart/service-php?#enable" target="_blank">link</a></p>
            <p>After the service is created, don't forget to enable it. Go to API Manager > Library > Analytics API > Enable </p>
          </div>
        </div>
      </div>
    </div>

    <div class="posts-most-viewed-box">
      <div class="postbox">
        <h2 class="hndle">Info for Google Analytics API Service </h2>
        <div class="inside">
          <div class="form-wrap">
            <div class="form-field">
              <label><strong>Upload the json file with the google api credentials:</strong></label>
              <input type="file" name="gapicredentials" />
              <p>The file must be a json</p>
            </div>
            <div class="form-field">
              <label><strong>Select Period of Time:</strong></label>
              <input type="number" name="gapi_period_time" min="2" value="<?php echo isset($_POST['gapi_period_time']) ? $_POST['gapi_period_time'] : ''?>"/>
              <p>Set the number of days the period should have, it must be higher than one day.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" name="form_provider_posts_most_viewed_action"  value="true"/>
  <input type="submit" value="Upload" class="button button-primary" />
</form>