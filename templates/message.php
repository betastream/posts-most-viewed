<?php if (isset($data['type']) && isset($data['msg'])) : ?>
  <div class="<?php echo $data['type'] ?> notice">
      <p><?php echo $data['msg'] ?></p>
  </div>
<?php endif ?>