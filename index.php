<?php

namespace Posts_Most_Viewed;

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/lib/Base.php';

/**
 * render html
 */
function render($file , $data = array() ){
    $template = __DIR__ . '/templates/'.$file.'.php';

    if(file_exists($template)){
        ob_start();
            $data;
            require $template;
        return ob_get_clean();
    }
}

/**
 * Enqueues posts most viewed assets
 */
function setup_assets_plugin(){
    wp_enqueue_script('jquery');
    wp_register_script('posts-most-viewed-js', plugin_dir_url( __FILE__ )   . 'assets/javascripts/posts-most-viewed.js' );
    wp_enqueue_script('posts-most-viewed-js');
    wp_enqueue_style( 'posts-most-viewed-style', plugin_dir_url( __FILE__ ) . 'assets/css/posts-most-viewed.css' );
}

/**
 *  Return posts most viewed
 */
function get_results($max_results = 4){
    require_once __DIR__.'/lib/Frontend.php';
    $frontend = Frontend::instance();
    return $frontend->results($max_results);
}

/**
 * Initializes admin stuf
 */
function admin_init(){
  if( is_admin() ){
    add_action('admin_enqueue_scripts', __NAMESPACE__. '\setup_assets_plugin');
    require_once __DIR__.'/lib/Admin.php';
    Admin::instance();
  }
}

add_action( 'init', __NAMESPACE__.'\admin_init', 1 );