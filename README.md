
## Posts most viewed

The **Post most viewed** is a wordpress plugin which allows you to get the posts that were most viewed in your site. This information will be obtained through providers such as Google Analytics.

### How to use in a wordpress theme

After installing and activating the plugin, use this function to get the posts most viewed:

```
\Posts_Most_Viewed\get_results($max_results);
```
The parameter **$max_results** determines how many posts should be return, the default value is 4

### Posts most viewed in the wp-admin

To go to the Posts most viewed section in the  wp-admin, click on the link Posts Most Viewed in the settings menu.

There are two main sections:

* The section to select the provider and to set up the provider selected. This section only appears if a provider isn't selected.
* The other section only appears if a provider has already been selected and here you can:
  * Check the provider status
  * Clear the cache
  * See the Provider Credentials
  * Delete Provider Data.

***

### Providers

As previously stated the post most viewed results are obtained through providers such as Google Analytics , Adobe Analytics or others. Currently this plugin only support one provider, Google Analytics. But you can add others providers.

#### Adding a Provider

First you need to create a Class, in `lib/providers`, where all the provider logic or code will be. The name of the Class must follow this convention `ProviderName_Provider`. This Class must extend from Base_Provider Class.

All the providers classes must have the method `results()`. This method basically returns an array with the provider results.

##### Example:

```
 return $result =  array(
     'data' => array()
  );
```

The **data key** is where all the results will be, this array may have other keys with another type of information.

This key (data) will be an array of arrays and the first value of this arrays must be the url of the post most viewed.

##### Example:

```

array(
    'data' => array(
         array(
           '/sustainability/better-certification-sustainability-standards-healthier-forests/'
         )
         array(
            '/energy-policy/how-does-europe-use-biomass/'
        )
    )
)
```

After creating the new Class go to the Admin.php Class, in the libs folder, to add the new provider. In the __construct of this Class, add your provider to the `$this->providers` array. The key of this new value must follow this convention `providername_provider`, and the value will be the provider label.

##### Example:

```
 public function __construct(){
        $this->providers = array(
            'google_provider'       => 'Google Analytics',
            'providername_provider' => 'Provider Name',
        );
        ...
}
```
In the same Class, add the method that will handle the new provider settings in wp-admin. The name of this method must follow this convention `handler_providername_provider`. This method must have at least the parameter `$provider`. Will be also in this method where the options , Current Provider and Provider Data, will be saved.


##### Example:

```
private function handler_providername_provider($provider) {
    .... other code ...

    // Save Current Provider
    update_option(self::POSTS_MOST_VIEWED_PROVIDER, $provider)

    // Save Provider Data
    update_option(self::POSTS_MOST_VIEWED_PROVIDER_DATA, serialize($data))

}
```

The admin html of the new provider should be placed in the `templates/provider_form_data.php` file inside the form  tag `<form>` that already exists and must be inside an element like this


```
<div data-posts-most-viewed-provider="providername_provider" class="posts-most-viewed-provider <?php echo ($data['selected_provider'] == 'providername_provider') ? 'selected' : '' ?>">
    ...Provider Html...
</div>
```

Summary:

* Create the Provider Class
* Add the new Provider into the Admin Class and also the method that will handle it's logic
* Add the Provider Html in the `templates/provider_form_data.php` file

**IMPORTANT:** respect the name convention in each steps otherwise it won't work.

####Provider Dependencies

All provider dependencies must be added into the composer.json