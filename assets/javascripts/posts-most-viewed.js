jQuery(function() {
  'use strict';
  var $ = jQuery;

  function postsMostViewSeeMore(event){
    event.preventDefault();
    $('.posts-most-viewed-see-more').slideToggle('slow');
  }


  function providerToggle(elem){
    var provider     = $(elem).val();
    var providerNode =  $('*[data-posts-most-viewed-provider="'+provider+'"]');

    if(providerNode.length){
      $('.posts-most-viewed-provider.selected').removeClass('selected');
      providerNode.addClass('selected');
    }
  }

  if($('.js-posts-most-viewed-providers').length){
    $('.js-posts-most-viewed-providers').on('change', function(){providerToggle(this)});
  }

  $(document).on('click', '.js-posts-most-viewed-see-more' , function (event){postsMostViewSeeMore(event)});
});