<?php

/**
 * Posts Most Viewed plugin
 *
 * @author Gonçalo Ribeiro <gribeiro@beatstream.co>
 * @package Top_Stories
 * @version 0.2.2
 */
/*
Plugin Name: Posts Most Viewed
Description: Get the most viewed posts based on providers like the google analytics api.
Version: 0.2.2
This file must be parsable by php 5.3
*/

if( !defined('ABSPATH') )
    exit;

register_activation_hook( __FILE__, create_function("", '$ver = "5.3"; if( version_compare(phpversion(), $ver, "<") ) die( "This plugin requires PHP version $ver or greater be installed." );') );

require __DIR__.'/index.php';
