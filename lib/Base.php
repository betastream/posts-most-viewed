<?php

namespace Posts_Most_Viewed;

class Base {

     /**
     * @var String The name of the wordpress option for the data
     */
    const POSTS_MOST_VIEWED_PROVIDER_DATA = 'posts_most_viewed_provider_data';

    /**
     * @var String The name of the wordpress option for provider
     */
    const POSTS_MOST_VIEWED_PROVIDER = 'posts_most_viewed_provider';

    /**
     * @var String The name of the posts most view results transient
     */
    const POSTS_MOST_VIEWED_RESULTS_TRANSIENT = 'posts_most_viewed_results_transient';


    /**
     * Get the posts most view
     *
     * @access protected
     * @since 0.1
     * @param String $provider Provider data
     * @param Array $data Provider data
     * @return Array
     */
    protected function get_most_viewed($provider , $data = array()){
        $provider      =  $this->capitalize_provider_name($provider);
        $provider_file =  __DIR__ . '/providers/'.$provider.'.php';

        if(file_exists($provider_file)){
            require_once $provider_file;

            $provider = "Posts_Most_Viewed\\".$provider;

            if(class_exists($provider)) {
               $current_provider  = new $provider($data);

               return $current_provider->results();
            }
            else{
                throw new \Exception('The Provider '.$provider.' doesn\'t exist (missing class)');
            }
        }
        else{
            throw new \Exception('The Provider '.$provider.' doesn\'t exist (missing file)');
        }
    }

    /**
     * Capitalize all words
     *
     * @access protected
     * @since 0.1
     * @param String $provider Provider Name
     * @return String
     */
    protected function capitalize_provider_name($provider){
        $provider = explode('_', $provider);
        $provider = array_map(function($provider_name){
            return ucfirst($provider_name);
        }, $provider);

        return  implode('_', $provider);
    }
}