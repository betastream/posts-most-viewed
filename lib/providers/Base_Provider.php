<?php
namespace Posts_Most_Viewed;

class Base_Provider {

    /**
     * Get Results from Provider
     *
     * @access protected
     * @since 0.1
     * @return Array
     */

    public function results() {
        return array(
            'data' => array()
        );
    }

     /**
     * Set a custom errors strutured from the Exceptions
     *
     * @access protected
     * @since 0.1
     * @param Array $errors Provider Errors
     * @return Array
     */
    protected function set_errors($errors){
        $error_msgs = array();

        if(is_array( $errors)){
            foreach ( $errors as $key => $value) {
                if(is_array($value)){
                    foreach ( $value as $k => $v) {
                        if($k === 'message'){
                            $error_msgs[] = $v;
                        }
                    }
                }
                elseif($key === 'message'){
                    $error_msgs[] = $value;
                }
            }
        }

        if(!count($error_msgs)){
            return array('error' => 'Provider Api Error');
        }
        else{
            return array('error' => implode(' , ', $error_msgs));
        }
    }
}