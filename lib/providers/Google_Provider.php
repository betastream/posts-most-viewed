<?php

namespace Posts_Most_Viewed;

require_once __DIR__.'/Base_Provider.php';

class Google_Provider extends Base_Provider{

    /**
    * @var Array  Gogogle Analytics Api Credentials
    */
    private $credentials;


    /**
    * @var Int period of time that should return
    */
    private $period_time;


    /**
     * Set the Gogogle Analytics Api Credentials and the period of time
     *
     * @access public
     * @since 0.1
     * @return void
     */
    public  function __construct($data){
        $this->period_time = (isset($data['period_time'])) ? $data['period_time'] : 7;
        $this->credentials = $data['credentials'];
    }


    /**
     * Get Results From the Gogogle Analytics Api
     *
     * @access protected
     * @since 0.1
     * @return Array
     */
    public function results() {
         try {
             $analytics  = $this->init_google_api($this->credentials);
             try {
                $profile    = $this->get_first_profile_id_google_api($analytics);
                $results    = $this->get_google_api_data($analytics, $profile);
                if(is_object($results) && property_exists($results, 'rows')){
                    return  array(
                        'data'       => $results->rows,
                        'extra_data' => $results
                    );
                }
                return array();
            }
            catch (\Google_Service_Exception $e){
                return $this->set_errors($e->getErrors());
            }
        }
        catch (\Exception $e) {
            return $this->set_errors(array('message' => $e->getMessage()));
        }
    }

     /**
     * Initializes the Gogogle Analytics Api
     *
     * @access protected
     * @since 0.1
     * @param Array $credentials Gogogle Analytics Api credentials
     * @return Object
     */
    protected function init_google_api($credentials) {

        $project = (isset($credentials['project_id'])) ? str_replace('-', ' ', $credentials['project_id']) : '';

        $client = new \Google_Client();
        $client->setApplicationName("Posts Most View From Project ".$project);
        $client->setAuthConfig($credentials);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        try {
            $analytics = new \Google_Service_Analytics($client);

            return $analytics;
        }
        catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }



     /**
     *  Get the user's first view (profile) ID.
     *  From a list of accounts for the authorized user
     *
     * @access protected
     * @since 0.1
     * @param Object $analytics Gogogle Analytics Api Object
     * @return Int
     */
    protected function get_first_profile_id_google_api($analytics) {
        $accounts = $analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
              $items = $properties->getItems();
              $firstPropertyId = $items[0]->getId();

              // Get the list of views (profiles) for the authorized user.
              $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstPropertyId);

              if (count($profiles->getItems()) > 0) {
                $items = $profiles->getItems();

                return $items[0]->getId();

                }
                else {
                    throw new \Exception('No views (profiles) found for this user.');
                }
            }
            else {
                throw new \Exception('No properties found for this user.');
            }
        }
        else {
            throw new \Exception('This user wasn\'t found in the google analytics account you want to access via Google Analytics API.');
        }
    }


  /**
   *  Calls the Core Reporting API and queries for the number of uniquePageviews
   *  for the last seven days.
   *
   * @access protected
   * @since 0.1
   * @param Object $analytics Gogogle Analytics Api Object
   * @param Int $profileId User Id From Gogogle Analytics Api
   * @return Object
   */

    protected function get_google_api_data($analytics, $profileId) {
        return $analytics->data_ga->get(
            'ga:' . $profileId,
            $this->period_time.'daysAgo',
            'today',
            'ga:uniquePageviews',
            array('dimensions'=>"ga:pagePath" , 'sort'=>"-ga:uniquePageviews")
        );
    }
}