<?php

namespace Posts_Most_Viewed;

class Frontend extends Base {

    /**
     * the instance of this class is set into this property
     *
     * @var Object
     */
    private static $instance;


     /**
     * Creates Instance of this Class
     *
     * @access public
     * @since 1.0
     * @return Object
     */
    public static function instance() {
      if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Frontend ) ) {
            self::$instance = new Frontend;
        }
        return self::$instance;
    }


    /**
     * Sets a data structure of the results
     *
     * @access public
     * @since 0.1
     * @param Int $max_results  Max number of results
     * @return Array
     */
    public function set_data_results($max_results){
        $count             = 0;
        $posts_most_viewed = array();
        $provider_data     = get_option(self::POSTS_MOST_VIEWED_PROVIDER_DATA);
        $provider          = get_option(self::POSTS_MOST_VIEWED_PROVIDER);

        if(false === $provider_data && false === $provider){
            return $posts_most_viewed;
        }

        $results = $this->get_most_viewed( $provider , unserialize($provider_data));

        if(isset($results['data']) && is_array($results['data'])){

            foreach ($results['data'] as $key => $value) {
                $url  = $value[0];

                if($count == $max_results){
                    break;
                }

                if(!$post_id =  url_to_postid( $url )){
                    continue;
                }

                if($post = get_post( $post_id )){
                    if($post->post_type === 'post'){
                        $posts_most_viewed[] = $post;
                        $count++;
                    }
                }
            }
        }

        return $posts_most_viewed;
    }

    /**
     * Creates the posts most viewed results transient
     *
     * @access public
     * @since 0.1
     * @param Int $max_results  Max number of results
     * @return Array
     */
    public function results($max_results = 10){

        if ( false === ( $posts_most_viewed = get_transient( self::POSTS_MOST_VIEWED_RESULTS_TRANSIENT ) ) ) {
            $posts_most_viewed = $this->set_data_results($max_results);
            set_transient( self::POSTS_MOST_VIEWED_RESULTS_TRANSIENT , $posts_most_viewed, 3 * HOUR_IN_SECONDS ); //3h
        }

        return $posts_most_viewed;
    }
}