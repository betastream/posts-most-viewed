<?php

namespace Posts_Most_Viewed;

class Admin extends Base {

    /**
     * The instance of this class is set into this property
     *
     * @var Object
     */
    private static $instance;

    /**
     * All Providers
     *
     * @var Array
     */
    private $providers;

    /**
     * Creates Instance of this Class
     *
     * @access public
     * @since 0.1
     * @return Object
     */
    public static function instance() {
      if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Admin ) ) {
            self::$instance = new Admin;
        }
        return self::$instance;
    }


    /**
     * Call the wordpress hooks and set providers
     *
     * @access private
     * @since 0.1
     * @return void
     */
    public function __construct(){
        // Each key of this array is the classname of the provider
        $this->providers = array(
            'google_provider' => 'Google Analytics'
        );

        $this->hooks();
    }


    /**
     * Set wordpress hooks
     *
     * @access private
     * @since 0.1
     * @return void
     */
    private function hooks() {
        add_action("admin_menu", array($this , 'submenu'));
    }


    /**
     * Creates the Posts Most Viewed submenu in the settings Menu
     *
     * @access public
     * @since 0.1
     * @return void
     */
    public function submenu(){
        add_submenu_page(
            'options-general.php',
            'Posts Most Viewed',
            'Posts Most Viewed',
            'manage_options',
            'posts-most-viewed',
            array($this , 'page_callback')
        );
    }


    /**
     * The callback for the page of the submenu Posts Most Viewed
     *
     * @access public
     * @since 0.1
     * @return void
     */
    public function page_callback(){
        $data                 = array();
        $provider_form_data   = array();
        $provider_data_result = $this->handler_provider_data();
        $provider_data        = get_option(self::POSTS_MOST_VIEWED_PROVIDER_DATA);
        $provider             = get_option(self::POSTS_MOST_VIEWED_PROVIDER);

        if( false === $provider_data  && false === $provider){
            $result = $this->handler_form_provider();

            if(is_array($provider_data_result)){
                $provider_form_data['message'] = \Posts_Most_Viewed\render('message' , $provider_data_result);
            }

            if(is_array($result)){
                $provider_form_data['message'] = \Posts_Most_Viewed\render('message' , $result);
            }

            if(is_array($result) && $result['type'] === 'updated'){
                $data['content'] = $this->get_settings_html($provider_form_data);
            }
            else{
                $provider_form_data['providers'] = $this->providers;

                reset($this->providers);
                $provider_form_data['selected_provider'] = (isset($_POST['post_most_viewed_providers']))
                    ? $_POST['post_most_viewed_providers']
                    : key($this->providers);

                $data['content'] = \Posts_Most_Viewed\render('provider_form_data' , $provider_form_data);
            }
        }
        else{
            $data['content'] = $this->get_settings_html(array(), $provider_data , $provider);
        }

        echo \Posts_Most_Viewed\render('main' , $data);
    }


    /**
     * Get the html when the Provider Settings are already defined
     *
     * @access private
     * @since 0.1
     * @param array $data data that could be need on the template settings
     * @param String $provider_data data about teh provider
     * @param String $provider current provider
     * @return String
     */
    private function get_settings_html($data = array(), $provider_data = false, $provider = false){

        $data['config'] = ($provider_data === false) ? unserialize(get_option(self::POSTS_MOST_VIEWED_PROVIDER_DATA)) : unserialize($provider_data);

        if($provider === false){
            $provider = get_option(self::POSTS_MOST_VIEWED_PROVIDER);
        }

        $delete_cache   = $this->delete_cache();
        $check_status   = $this->check_status($provider , $data['config']);

        if(is_array($delete_cache)){
            $data['message'] = \Posts_Most_Viewed\render('message' , $delete_cache);
        }

        if(is_array($check_status)){
            $data['status'] = $check_status;
        }

        return \Posts_Most_Viewed\render('settings' , $data);
    }


    /**
     * Handler the Provider Data
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function handler_provider_data(){
        if(isset($_POST['delete_posts_most_viewed_provider_data_action']) && $_POST['delete_posts_most_viewed_provider_data_action']== true ){

            if(!delete_option( self::POSTS_MOST_VIEWED_PROVIDER )){
                return array('msg' => 'It Wasn\'t Possible To Delete The Provider.<br> Please try again.', 'type' => 'error');
            }

            if(!delete_option( self::POSTS_MOST_VIEWED_PROVIDER_DATA )){
                return array('msg' => 'It Wasn\'t Possible To Delete The Provider Data.<br> Please try again.', 'type' => 'error');
            }

            return array('msg' => 'Provider Data Deleted With Success', 'type' => 'updated');
        }
    }


    /**
     * Handler the delete of the posts most viewed results transient
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function delete_cache(){
        if(isset($_POST['delete_cache_posts_most_viewed_action']) && $_POST['delete_cache_posts_most_viewed_action']== true ){

            if(!delete_transient(self::POSTS_MOST_VIEWED_RESULTS_TRANSIENT)){
                return array('msg' => 'It Wasn\'t Possible To Delete The Cache.<br> Please try again', 'type' => 'error');
            }

            return array('msg' => 'Cache Deleted With Success', 'type' => 'updated');
        }
    }


    /**
     * Check the Api status of the currrent provider
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function check_status($provider , $data){
        if(isset($_POST['check_status_posts_most_viewed_action']) && $_POST['check_status_posts_most_viewed_action']== true ){
            $status = array();
            $result = $this->get_most_viewed($provider , $data);

            if(is_array($result) && array_key_exists('error', $result)){
                $status =  array('msg' => $result['error'] , 'type' => 'error');
            }
            else{
                $status =  array('msg' => $result , 'type' => 'success');
            }

            return $status;
        }
    }

    /**
     * Handler From Provider
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function handler_form_provider() {
        if(isset($_POST['form_provider_posts_most_viewed_action']) && $_POST['form_provider_posts_most_viewed_action']== true ){
           $provider = $_POST['post_most_viewed_providers'];
           $method   = 'handler_' . $provider;

            if( method_exists($this, $method) ){
                return call_user_func(array( $this, $method) , $provider);
            }
            else{
                return array('msg' => 'Method '.$method .' doesn\'t exist', 'type' => 'error');
            }
        }
    }

    /**
     * Handler the form of google analytics api
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function handler_google_provider($provider) {
        if(true !== ($validation = $this->upload_file_validation($_FILES['gapicredentials']))){
            return $validation;
        }

        if( $_FILES["gapicredentials"]['type']!== 'application/json'){
            return array('msg' => 'File Must Be Type Json', 'type' => 'error');
        }

        $json = file_get_contents($_FILES["gapicredentials"]["tmp_name"]);

        if (!$config = json_decode($json, true)) {
             return array('msg' => 'Invalid Json For Auth Config', 'type' => 'error');
        }

        if(!isset($config['client_id'])){
            return array('msg' => 'Invalid Client Id' , 'type' => 'error');
        }

        $data = array('credentials' => $config);

        try {
            $result = $this->get_most_viewed($provider , $data);
        }
        catch (\Exception $e) {
            return array('msg' =>  $e->getMessage() , 'type' => 'error');
        }

        if(is_array($result) && array_key_exists('error', $result)){
            return array('msg' => $result['error'] , 'type' => 'error');
        }

        if(isset($_POST['gapi_period_time']) && $_POST['gapi_period_time'] > 1 && preg_match('/^\d+$/', $_POST['gapi_period_time'])){
           $data['period_time'] = $_POST['gapi_period_time'];
        }
        else{
            return array('msg' => 'Period Time invalid value' , 'type' => 'error');
        }

        if(! update_option(self::POSTS_MOST_VIEWED_PROVIDER, $provider)){
            return array('msg' => 'It Wasn\'t Possible To Save The Provider Selected .<br> Please try again' , 'type' => 'error');
        }

        if(! update_option(self::POSTS_MOST_VIEWED_PROVIDER_DATA, serialize($data))){
            return array('msg' => 'It Wasn\'t Possible To Save the Google Api Data.<br> Please try again' , 'type' => 'error');
        }

        return array('msg' => 'Provider Data Saved With Success', 'type' => 'updated');
    }


    /**
     *  Files upload Validation
     *
     * @access private
     * @since 0.1
     * @return Array
     */
    private function upload_file_validation($file){
        if($file['error'] !== 0){
            switch ($file['error']) {
                case 1:
                    $message = "The Uploaded File Exceeds The Upload_max_filesize";
                    break;
                case 2:
                    $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                    break;
                case 3:
                    $message = "The uploaded file was only partially uploaded";
                    break;
                case 4:
                    $message = "No file was uploaded";
                    break;
                case 5:
                    $message = "Missing a temporary folder";
                    break;
                case 6:
                    $message = "Failed to write file to disk";
                    break;
                case 7:
                    $message = "File upload stopped by extension";
                    break;

                default:
                    $message = "Unknown upload error";
                    break;
            }

            return array('msg' => $message , 'type' => 'error');
        }
        return true;
    }

}